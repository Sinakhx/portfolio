import React, { Component } from "react";
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Link
} from "react-router-dom";
import Profile from "../Profile/Profile";
import Website from "../Website/Website";
import Panle from "../Panel/Panle";
import { Button, Layout } from "antd";

const { Header, Content, Footer } = Layout;
class App extends Component {
    render() {
        return (
            <Router>
                <Layout
                    style={{
                        display: "flex",
                        flexDirection: "column",
                        minHeight: "100vh"
                    }}
                >
                    <Header
                        style={{
                            display: "flex",
                            justifyContent: "center",
                            flexGrow: 0,
                            alignItems: "center"
                        }}
                    >
                        <Button type="danger">
                            <Link to="/profile">
                                Profile
                            </Link>
                        </Button>
                        <Button type="primary">
                            <Link to="/website">
                                website
                            </Link>
                        </Button>
                        <Button type="warning">
                            <Link to="/panel">
                                Panel</Link>  
                        </Button>
                    </Header>
                    <Content style={{ flexGrow: 1 }}>
                        <Switch>
                            <Route path="/profile">
                                <Profile />
                            </Route>
                            <Route path="/website">
                                <Website />
                            </Route>
                            <Route path="/panel">
                                <Panle />
                            </Route>
                        </Switch>
                    </Content>
                </Layout>
            </Router>
        );
    }
}

export default App;
