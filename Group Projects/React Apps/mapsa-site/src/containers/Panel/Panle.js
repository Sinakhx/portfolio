import React, { Component } from 'react';
import {
    Route,
    Switch,
    Link
} from "react-router-dom";
import { Layout, Menu, Breadcrumb, Icon } from 'antd';
import AddCourse from './AddCourse';

const { SubMenu } = Menu;
const { Content, Footer, Sider } = Layout;


class Panle extends Component {


    render() {
        return (
            <>
                <Content style={{ padding: '0 50px' }}>
                    <Layout style={{ padding: '24px 0', background: '#fff' }}>
                        <Sider width={200} style={{ background: '#fff' }}>
                            <Menu
                                mode="inline"
                                theme="dark"
                                defaultSelectedKeys={['1']}
                                defaultOpenKeys={['sub1']}
                                style={{ height: '100%' }}
                            >
                                <SubMenu
                                    key="sub1"
                                    title={
                                        <span>
                                            <Icon type="user" />
                                            دوره ها
                                            </span>
                                    }
                                >
                                    <Menu.Item key="5">
                                        <Link to="/panel/addcourse">افزودن</Link>
                                    </Menu.Item>

                                    <Menu.Item key="6">لیست دوره ها</Menu.Item>
                                </SubMenu>


                            </Menu>
                        </Sider>
                        <Content style={{ padding: '0 24px' }}>
                    <Breadcrumb style={{ margin: '16px 0' }}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>List</Breadcrumb.Item>
                        <Breadcrumb.Item>App</Breadcrumb.Item>
                    </Breadcrumb>
                            <Switch>
                                <Route path="/panel/addcourse">
                                    <AddCourse />
                                </Route>
                            </Switch>
                        </Content>
                    </Layout>
                </Content>
                {/* <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer> */}
            </>


        )
    }
}

export default Panle;