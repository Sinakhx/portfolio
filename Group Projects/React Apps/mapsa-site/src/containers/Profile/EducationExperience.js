import React from "react";
import style from "./educationExperience.module.css";
import { Row, Col } from "antd";

class EducationExperience extends React.Component {
    render() {
        return (
            <div>
                <Row className={style.mainRow}>
                    <Col className={style.leftPanel} span={12}>
                        <h3 className={style.mainTitle}>تحصیلات</h3>
                        <div className={style.boxBorder}></div>
                        <div className={style.education}></div>
                    </Col>
                    <Col className={style.rightPanel} span={12}>
                        <h3 className={style.mainTitle}>تجربیات کاری</h3>
                        <div className={style.boxBorder}></div>
                        <div className={style.experience}></div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default EducationExperience;
