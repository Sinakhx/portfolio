

// ******** Html collection of cards ************************
const cardsOfrow = [...document.getElementsByClassName('cards-row')];
const cardsOfcol = [...document.getElementsByClassName('cards-column')];

// ******************* shuffle cards ************************
let arr = []
for(i=0; i<=17; i++){
	arr.push(i)
	arr.push(i)
}
shuffle(arr)

for(i=0; i<=17; i++){
	cardsOfcol[i].style.order = '' + arr[i]
	cardsOfrow[i].style.order = '' + arr[i]
}


// ****************** Array that stores clicked cards ****************

let clickedCards = [];
let cout_faded_cards = 0;
const num_of_cards = 36;

function check(id) {
	const elem = document.getElementById(id)
	const len = clickedCards.length || 0;

	//do nothing when a faded card is clicked again
	if (elem.style.opacity == '0') {
		return
	}

	if (len == 0) {
		clickedCards.push(id);
		flipFront(id)
	}
	else if (len == 1) {
		clickedCards.push(id);
		flipFront(id)
		//compare if images are equal & cards are different
		const compare = ((document.getElementById(clickedCards[0]).dataset.img == document.getElementById(clickedCards[1]).dataset.img)
			&& (clickedCards[0] != clickedCards[1]))

		//if not having the same images
		if (!compare) {
			setTimeout(() => {
				flipBack(clickedCards[0])
				flipBack(clickedCards[1])
				clickedCards = []
			}, 800);
			//console.log('not matched');

		}
		//if having the same images
		else if (compare) {
			fadeOutEffect(clickedCards[0])
			fadeOutEffect(clickedCards[1])

			clickedCards = []
			cout_faded_cards += 2
			if (cout_faded_cards == num_of_cards) {
				document.getElementById('end').style.display = 'block';
				getInfo()
				btn.click();
				console.log('Game Over: You Win!');
				return
			}

			//console.log('matched');
		}
		//if the player is clicking fast
	} else if (len > 1) {
		console.log('too fast');
		flipBack(clickedCards[0])
		flipBack(clickedCards[1])
	}

}


// **************************** flip ********************************************************

function flipFront(id) {
	const node = document.getElementById(id);
	node.style.transform = 'rotateY(180deg)'
}
function flipBack(id) {
	const node = document.getElementById(id);
	node.style.transform = 'rotateY(0deg)';
}


// ******************************************* timer *****************************************
let minutesLabel = document.getElementById('minutes');
let secondsLabel = document.getElementById('seconds');
let totalSeconds = 0;
let s, m;

// ************* store seconds and minutes in object **************
let maxCountOfTime = []
let timeCount = setInterval(setTime, 1000);

function setTime() {
	++totalSeconds;
	secondsLabel.innerHTML = pad(totalSeconds % 60);
	minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
	s = parseInt(secondsLabel.innerText);
	m = parseInt(minutesLabel.innerText);
	maxCountOfTime = [m, s];
	localStorage.setItem('timer', JSON.stringify(maxCountOfTime))
}
function pad(value) {
	let valString = value + '';
	if (valString.length < 2) {
		return '0' + valString;
	} else {
		return valString;
	}
}

function getInfo() {
	const info = [];
	let modalContent = ''

	let getName = prompt('please enter your name : ');
	if (getName != null) {
		info.push(getName)
		localStorage.setItem('name', JSON.stringify(info))
	}
	const timer = JSON.parse(localStorage.getItem('timer'))
	const name = JSON.parse(localStorage.getItem('name'))
	// console.log(name,timer)
	modalContent = `
		<p>Dear ${name}</p>
		<p>Your point is : 100%</p>
		<p>Time : ${timer[0] + ':' + timer[1]}</p>
	`
	document.getElementById('result-model').innerHTML = modalContent;
	clearInterval(timeCount)
}

// ********************** modal box ******************************
var modal = document.getElementById("myModal");
var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("close")[0];

btn.onclick = function () {
	modal.style.display = "block";
}

span.onclick = function () {
	modal.style.display = "none";
}
window.onclick = function (event) {
	if (event.target == modal) {
		modal.style.display = "none";
	}
}


// *********************** Fade out effect **************************
function fadeOutEffect(id) {
	var fadeTarget = document.getElementById(id);
	var fadeEffect = setInterval(function () {
		if (!fadeTarget.style.opacity) {
			fadeTarget.style.opacity = 1;
		}
		if (fadeTarget.style.opacity > 0) {
			fadeTarget.style.opacity -= 0.1;
		} else {
			clearInterval(fadeEffect);
		}
	}, 30);
}

// ********** Fisher–Yates shuffle algorithm for arrays ***************
function shuffle(array) {
    var m = array.length, t, i;

    // While there remain elements to shuffle…
    while (m) {

        // Pick a remaining element…
        i = Math.floor(Math.random() * m--);

        // And swap it with the current element.
        t = array[m];
        array[m] = array[i];
        array[i] = t;
    }

    return array;
}