
const rows = document.getElementById('rows')
const black_white_rows = document.getElementById('black-white-rows')

let j = 1, k = 1
for (let i = 1; i <= 12; i++) {
    rows.innerHTML += `
    <div class="game-rows" id="row${i}">
        <div class="main-row-circles" id="${j++}" onclick="pickBox('${j - 1}')"></div>
        <div class="main-row-circles" id="${j++}" onclick="pickBox('${j - 1}')"></div>
        <div class="main-row-circles" id="${j++}" onclick="pickBox('${j - 1}')"></div>
        <div class="main-row-circles" id="${j++}" onclick="pickBox('${j - 1}')"></div>
        <div class="main-row-circles" id="${j++}" onclick="pickBox('${j - 1}')"></div>
    </div>
    `
    black_white_rows.innerHTML += `
    <div class="black-white-box" id="bw-row${i}">
        <div class="black-white-btn" id="bw${k++}"></div>
        <div class="black-white-btn" id="bw${k++}"></div>
        <div class="black-white-btn" id="bw${k++}"></div>
        <div class="black-white-btn" id="bw${k++}"></div>
        <div class="black-white-btn" id="bw${k++}"></div>
    </div>
    `
}


const colors = ["brown", "purple", "blue", "red", "yellow", "green", "pink", "orange"]
const mastersChoice = []
let ind;
for (i = 1; i <= 5; i++) {
    ind = Math.floor(Math.random() * 5)
    mastersChoice.push(colors[ind])
}


let usersChoice = ['', '', '', '', '']
let pickedColor = '',
    pickedBox = '',
    activeRow = 12;



function pickColor(id) {
    pickedColor = id
    console.log(id);
    return id

}

function pickBox(id) {
    pickedBox = document.getElementById(id)
    //console.log(id);

    //check if the clicked box is in the active row
    const isActive = (id <= (activeRow * 5)) && (id > (activeRow * 5 - 5))
    //console.log(`num is: ${isActive}`);
    if (!isActive) return 0

    //change the backgroundColor
    if (colors.includes(pickedColor)) {
        pickedBox.style.backgroundColor = pickedColor

        //push to the array
        const index = (id % (activeRow * 5) + 4) % 5
        //console.log(`index is ${index}`);
        usersChoice[index] = pickedColor
    }

    return pickedBox

}

let tj = 1000;
let tk = 200000;

function onSubmit() {
    const submit = document.getElementById('submit')
    if (usersChoice.includes('')) {
        alert('choose 5 colors for the row')
        return
    }

    //on submit, change the row
    activeRow -= 1
    if (activeRow < 1) {
        alert('you lost')
        return
    }

    //check if the player wins
    let ans = 0
    for (i = 0; i <= 4; i++) {
        if (usersChoice[i] == mastersChoice[i]) ans++
    }
    if (ans == 5) {
        alert('you win')
        return
    }

    //check for black & white
    let black = 0;
    let white = 0;
    // function countbws(bw){
    //     for (i=0; i<=4; i++){
    //         //console.log(`------------- i = ${i} -------------------`)
    //         if (usersChoice[i] === mastersChoice[i]) bw++;
    //         //console.log(`in ${bw}: usersChoice[${i}] = ${usersChoice[i]} & \n mastersChoice[${i}] = ${mastersChoice[i]}`)
    //         //console.log(`bw = ${bw}`);

    //         //count whites
    //         white += count(usersChoice[i], mastersChoice)
    //     }
    //     return bw
    // }
    // //count blacks
    // black = countbws(black)

    // white -= black


    

    console.log(mastersChoice)
    //find blacks
    console.log(`find blacks:`)
    let master_temp = [...mastersChoice]
    console.log(`master_temp: ${master_temp}`)
    let user_temp = [...usersChoice]
    console.log(`user_temp: ${user_temp}`)
    for (i = 0; i <= 4; i++) {
        if (user_temp[i] === master_temp[i]) {
            black++;
            console.log(`black: ${black}`)
            user_temp[i] = tj++
            console.log(`user_temp: ${user_temp}`)
            master_temp[i] = tj++
            console.log(`master_temp: ${master_temp}`)
        }
    }

    //find whites
    console.log(`------------------------------------`)
    console.log(`find whites:`)
    for (i = 0; i < master_temp.length; i++) {
        let temp_index = user_temp.indexOf(master_temp[i])
        console.log(`temp_index: ${temp_index}`);
        if ( temp_index != -1 ){
            white++
            console.log(`white: ${white}`);
            user_temp[temp_index] = tk++
            console.log(`user_temp: ${user_temp}`);
        }
    }



    console.log(`final white: ${white}`)
    console.log(`final black: ${black}`)


    console.log('--------------------- end -----------------------');
    //show white & blacks
    const bottom_range = (activeRow * 5 + 1)
    const upper_range = (activeRow + 1) * 5

    for (i = bottom_range; i <= upper_range; i++) {
        let bw = document.getElementById(`bw${i}`)
        if (white > 0) {
            bw.style.backgroundColor = 'white'
            white--

        }
        else if (black > 0) {
            bw.style.backgroundColor = 'black'
            black--
        }
    }
    // empty the array
    usersChoice = ['', '', '', '', '']
    console.log(mastersChoice)
}


function count(sth, arr) {
    let count = 0;
    for (i = 0; i < arr.length; i++) {
        if (sth === arr[i]) count++
    }
    return count
}
//console.log(count(user[0], master))