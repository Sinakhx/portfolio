const numberOfRows = 3;
const numberOfCols = 6;

//render HTML
//inner lines & rows
let inner = "";

for (let i = 0; i < numberOfRows; i++) {
  inner += `<div class="row mid-row">
<div class="col d-flex flex-row-reverse">
<div class="dot"></div>`;

  for (let j = 0; j < numberOfCols; j++) {
    inner += `<div class="horizontal" onclick="colorize(this)" data-row="${i}" data-col="${j}"></div>
  <div class="dot"></div>`;
  }
  inner += `</div></div>`;

  inner += `<div class="row main-row">
  <div class="col d-flex flex-row-reverse">`;

  for (let j = 0; j < numberOfCols; j++) {
    inner += `<div class="vertical" onclick="colorize(this)" data-row="${i}" data-col="${j}"></div>
    <div class="square" data-row="${i}" data-col="${j}"></div>`;
  }

  inner += `<div class="vertical" onclick="colorize(this)" data-row="${i}" data-col="${numberOfCols}" onclick="colorize(this)"></div>
        </div>
        </div>
        <div class="row mid-row">
        <div class="col d-flex flex-row-reverse">
    </div></div>`;
}

//the last line
inner += `<div class="row mid-row">
<div class="col d-flex  flex-row-reverse">
  <div class="dot"></div>`;

for (let j = 0; j < numberOfCols; j++) {
  inner += `<div class="horizontal" onclick="colorize(this)" data-row="${numberOfRows}" data-col="${j}" onclick="colorize(this)"></div>
    <div class="dot"></div>`;
}

inner += `</div></div>`;
document.getElementById("main").innerHTML += inner;

//check logic
let turn = "blue",
  blue_score = 0,
  red_score = 0,
  keepTurn = false;

function colorize(element) {
  // if already colorized, return
  if (isFilled(element)) return;
  //change element's color
  element.style.backgroundColor = turn;
  //add filled classlist to prevent furthur color changes
  element.classList.add("filled");
  //check if the player gets points

  check();
  //change turns
  if (!keepTurn) {
    turn = turn === "blue" ? "red" : "blue";
  } else {
    keepTurn = false;
  }
  //show the score board
  const blu = document.getElementById("blue"),
    rd = document.getElementById("red");
  blu.innerHTML = blue_score;
  rd.innerHTML = red_score;

  blu.classList.remove("glow");
  rd.classList.remove("glow");
  document.getElementById(turn).classList.add("glow");
}

function isFilled(element) {
  return element.className.split(" ").includes("filled");
}

function getElem(direction, row, col) {
  return document.querySelector(
    `.${direction}[data-row="${row}"][data-col="${col}"]`
  );
}

function check() {
  let top, left, bottom, right, square;
  for (i = 0; i < numberOfRows; i++) {
    for (j = 0; j < numberOfCols; j++) {
      top = getElem("horizontal", i, j);
      left = getElem("vertical", i, j);
      bottom = getElem("horizontal", i + 1, j);
      right = getElem("vertical", i, j + 1);
      square = getElem("square", i, j);
      //skip the filled squares
      if (isFilled(square)) continue;
      //check for points
      if (
        isFilled(top) &&
        isFilled(left) &&
        isFilled(bottom) &&
        isFilled(right)
      ) {
        //if ok, fill the square & add filled class to it & give score & keep the turn
        square.style.backgroundColor = turn;
        square.classList.add("filled_bordered");
        square.classList.add("filled");
        //give points to the scoring player
        turn === "blue" ? blue_score++ : red_score++;
        keepTurn = true;
      }
    }
  }
}
