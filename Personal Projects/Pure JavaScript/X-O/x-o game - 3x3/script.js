const filled = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
let counter = 0
let winner = ''


function check(id) {
    const box = document.getElementById(id)
    //if the clicked box is already full or if the winner is defined, do nothing 
    if ((box.innerHTML != '') || (winner != ''))
        return

    //put 'X' or 'O' in the box
    const user = (counter % 2 == 0) ? 'O' : 'X';
    counter++
    box.innerHTML = user;

    //fill the array of filled elements
    const intId = parseInt(id) - 1
    const row = Math.floor(intId / 3)
    const col = intId % 3
    filled[row][col] = user

    //check if the user wins
    isWon(user)
}


function isWon(user) {
    //check if there are triples in a 'row'
    for (let i = 0; i <= 2; i++) {
        let success = true
        for (let j = 0; j <= 1; j++) {
            if (filled[i][j] != filled[i][j + 1]) {
                success = false
                break
            }
        }
        if (isOver(user, success))
            return
    }

    //check if there are triples in a 'column'
    for (let j = 0; j <= 2; j++) {
        let success = true
        for (let i = 0; i <= 1; i++) {
            if (filled[i][j] != filled[i + 1][j]) {
                success = false
                break
            }
        }
        if (isOver(user, success))
            return
    }

    //check if there are triples in the 1st 'diagonal'
    let success = true
    for (let i = 0; i <= 1; i++) {
        if (filled[i][i] != filled[i + 1][i + 1]) {
            success = false
            break
        }
    }
    if (isOver(user, success))
        return

    //check if there are triples in the 2nd 'diagonal'
    success = true
    for (let i = 0; i <= 1; i++) {
        if (filled[i][2 - i] != filled[i + 1][1 - i]) {
            success = false
            break
        }
    }
    if (isOver(user, success))
        return

    //check if the game is drawn
    let draw = 0
    filled.forEach(row =>{
        row.forEach(box =>{
            if (box == 'X' || box == 'O') {
                draw++
            }
        })
    })
    if (draw == 9){
        setTimeout(() => {
            alert('the game is drawn')
        }, 200);
        return
    }
}

function isOver(user, success) {
    //console.log(`success = ${success}`);
    if (success) {
        //console.log(`player ${user} wins!`);
        setTimeout(() => {
            alert(`player ${user} wins!`);
        }, 200);
        winner = user
        return true
    }
}