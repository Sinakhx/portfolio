import React, { useState, useEffect } from "react";
import TodoForm from "./TodoForm";
import List from "./List";
import LastButtons from "./LastButtons";

function Todo() {
  const initialTodos = [
    { id: 1, task: "مطالعه", done: false },
    { id: 2, task: "خوابیدن", done: false },
    { id: 3, task: "غذا خوردن", done: false },
    { id: 4, task: "ورزش کردن", done: false },
    { id: 5, task: "مسواک زدن", done: false },
    { id: 6, task: "درخت کاشتن", done: false }
  ];

  const [todos, setTodos] = useState(
    JSON.parse(localStorage.getItem("todoList")) || initialTodos
  );

  useEffect(() => {
    localStorage.setItem("todoList", JSON.stringify(todos));
  }, [todos]);

  const addTodo = newTodoText => {
    let newId = Math.max(...todos.map(todo => todo.id)) + 1;
    newId = newId < 0 ? 1 : newId;
    setTodos([...todos, { id: newId, task: newTodoText, done: false }]);
  };

  const deleteItem = item => {
    setTodos(todos.filter(todo => todo !== item));
  };

  const doUndo = (item, index, bool) => {
    const newItem = { ...item, done: bool };
    const newTodos = todos
      .filter((obj, ind) => index !== ind)
      .concat(newItem)
      .sort((a, b) => a.id - b.id);
    setTodos(newTodos);
  };

  const hasdone = () => {
    return todos.map(item => item.done).reduce((a, b) => a + b, 0);
  };

  const exportTodos = () => {
    alert(localStorage.getItem("todoList"));
  };

  const importTodos = () => {
    const imp = JSON.parse(
      prompt(
        "لطفاً لیست خود را به صورت آرایه ای از اشیاء استاندارد وارد نمایید"
      )
    );
    try {
      if (imp && imp.length > 0) setTodos(imp);
    } catch (error) {
      window.location.reload();
    }
  };

  const delTodos = () => {
    const newTodos = todos.filter(item => item.done === false);
    setTodos(newTodos);
  };

  const resetTodos = bool => {
    const newTodos = [];
    todos.forEach(item => {
      const newItem = { ...item, done: bool };
      newTodos.push(newItem);
    });
    setTodos(newTodos);
  };

  const handleEdit = (item, index) => {
    const newTask = prompt("نام جدید را وارد نمایید", item.task);
    const newItem = { ...item, task: newTask ? newTask : item.task };
    doUndo(newItem, index, item.done);
  };

  const moveUpDown = (item, index, direction) => {
    const swap = () => {
        // swap ids & update the state & return
        const item1 = { ...todos[ind], id: item.id };
        const item2 = { ...item, id: todos[ind].id };
        const newTodos = todos
          .filter(obj => obj.id !== todos[ind].id)
          .filter(obj => obj.id !== item.id)
          .concat(item1)
          .concat(item2)
          .sort((a, b) => a.id - b.id);
        return setTodos(newTodos);
    }
    let ind = index;
    if (direction === "up") {
      while (ind > 0) {
        ind--;
        if (todos[ind].done === item.done) {
          swap();
          break;
        }
      }
    } else {
      while (ind < todos.length - 1) {
        ind++;
        if (todos[ind].done === item.done) {
          swap();
          break;
        }
      }
    }
  };

  return (
    <div id="container">
      <h1>لیست کارها</h1>
      <TodoForm addtodo={addTodo} />
      {hasdone() !== todos.length && <h3>کارهای در دست انجام:</h3>}
      <List
        completed={false}
        tasks={todos}
        onEdit={handleEdit}
        onDelete={deleteItem}
        onDo={doUndo}
        onMove={moveUpDown}
      />
      {!!hasdone() && <h3>کارهای انجام شده:</h3>}
      <List
        completed={true}
        tasks={todos}
        onEdit={handleEdit}
        onDelete={deleteItem}
        onDo={doUndo}
        onMove={moveUpDown}
      />
      <LastButtons
        set={exportTodos}
        get={importTodos}
        clear={delTodos}
        reset={() => resetTodos(false)}
        done={() => resetTodos(true)}
      />
    </div>
  );
}

export default Todo;
