import React, { Component } from "react";
import { Battery } from "../assets/svg/Battery";
import { Signal } from "../assets/svg/Signal";
import { WifiIcon } from "../assets/svg/WifiIcon";
import { BurgerMenu } from "../assets/svg/BurgerMenu";
import { SearchIcon } from "../assets/svg/SearchIcon";
import { VideoPlayerIcon } from "../assets/svg/VideoPlayerIcon";
import { StarIcons } from "../assets/svg/StarIcons";

class Header extends Component {
  render() {
    return (
      <div className="header">
        <div className="expressTrain"></div>
        <div className="head-icons">
          <div className="top-container">
            <div className="first-icons">
              <div><Battery /><WifiIcon /><Signal /> </div>
              <span className="time">12:14</span>
            </div>
            <div className="second-icons">
                <BurgerMenu />
                <SearchIcon />
            </div>
          </div>
          <VideoPlayerIcon />
          <div className="description">
              <div className="h-title">قتل در قطار سریع السیر شرق</div>
              <span className="h-comments">۱۸۹ نظر</span>
              <StarIcons />
              <div></div>
              <button className="h-btn">اکشن</button>
              <button className="h-btn">ترسناک</button>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
