import React, { Component } from "react";
import MovieCard from "./MovieCard";
import Swiper from "react-id-swiper";

class Slider1 extends Component {
  state = {
    movies: [
      {
        title: "مهتاب",
        comments: "۲۹۵",
        src: "/img/Thumb.png"
      },
      {
        title: "جنگ ستارگان",
        comments: "۳۱۲",
        src: "/img/Thumb 1.png"
      },
      {
        title: "دیپ واتر هوریزن",
        comments: "۲۹۵",
        src: "/img/Thumb 2.png"
      },
      {
        title: "شکل آب",
        comments: "۱۸۴",
        src: "/img/Thumb 3.png"
      },
      {
        title: "لوگان",
        comments: "۲۸۹",
        src: "/img/Thumb 5.png"
      }
    ],
    params: {
      slidesPerView: this.props.width < 400 ? 1.4 : 3,
      spaceBetween: 8,
      autoplay: {
        delay: 3800,
        disableOnInteraction: false
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true
      }
    }
  };

  componentWillReceiveProps(props) {
    if (props.width !== this.props.width) {
      console.log('1 is:',1);
      this.setState(state => {
        return {
          params: {
            ...state.params,
            slidesPerView: props.width < 400 ? 1.4 : 3
          }
        };
      });
    }
  }

  render() {
    
    const { params, movies } = this.state;
    
    return (
      <Swiper {...params}>
        {movies.map(({ src, comments, title }, index) => (
          <div key={index}>
            <MovieCard
              width="100%"
              image={src}
              comments={comments}
              title={title}
              stars={true}
            />
          </div>
        ))}
      </Swiper>
    );
  }
}

export default Slider1;
