import React, { Component } from "react";
import MovieCard from "./MovieCard";
import Swiper from "react-id-swiper";

class Slider2 extends Component {
  state = {
    movies: [
      {
        title: "دیپ واتر هوریزن",
        comments: "۲۹۵",
        src: "/img/Thumb 2.png"
      },
      {
        title: "شکل آب",
        comments: "۱۸۴",
        src: "/img/Thumb 3.png"
      },
      {
        title: "Un Beau Soleil Interieur",
        comments: "۲۸۹",
        src: "/img/Thumb 4.png"
      },
      {
        title: "مهتاب",
        comments: "۲۹۵",
        src: "/img/Thumb.png"
      },
      {
        title: "جنگ ستارگان",
        comments: "۳۱۲",
        src: "/img/Thumb 1.png"
      }
     
      
    ],
    params: {
      slidesPerView: 3,
      spaceBetween: 8,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true
      }
    }
  };

  render() {
    const { params, movies } = this.state;
    return (
      <Swiper {...params}>
        {movies.map(({ src, comments, title }, index) => (
          <div key={index}>
            <MovieCard
              width="100%"
              image={src}
              comments={comments}
              title={title}
              stars={false}
            />
          </div>
        ))}
      </Swiper>
    );
  }
}

export default Slider2;
