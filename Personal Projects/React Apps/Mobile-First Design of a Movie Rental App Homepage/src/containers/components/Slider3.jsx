import React, { Component } from "react";
import MovieCard from "./MovieCard";
import Swiper from "react-id-swiper";

class Slider3 extends Component {
  state = {
    movies: [
      {
        title: "لوگان",
        comments: "۲۸۹",
        src: "/img/Thumb 5.png"
      },
      {
        title: "دانکرک",
        comments: "۲۸۹",
        src: "/img/Thumb 6.png"
      },
      {
        title: "خاطره",
        comments: "۲۸۹",
        src: "/img/Thumb 7.png"
      },
      {
        title: "لوگان",
        comments: "۲۸۹",
        src: "/img/Thumb 5.png"
      },
      {
        title: "دانکرک",
        comments: "۲۸۹",
        src: "/img/Thumb 6.png"
      },
      {
        title: "خاطره",
        comments: "۲۸۹",
        src: "/img/Thumb 7.png"
      }
     
      
    ],
    params: {
      slidesPerView: 3,
      spaceBetween: 8,
      pagination: {
        el: ".swiper-pagination",
        clickable: true
      }
    }
  };

  render() {
    const { params, movies } = this.state;
    return (
      <Swiper {...params}>
        {movies.map(({ src, comments, title }, index) => (
          <div key={index}>
            <MovieCard
              width="100%"
              image={src}
              comments={comments}
              title={title}
              stars={false}
            />
          </div>
        ))}
      </Swiper>
    );
  }
}

export default Slider3;
