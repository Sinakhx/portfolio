import React, { Component } from "react";
import Slider1 from "./Slider1";
import Slider2 from "./Slider2";
import Slider3 from "./Slider3";

class OnTheatre extends Component {
  state = {
    width: window.innerWidth
  }

  updateDimensions() {
      this.setState({ width: window.innerWidth });
  }
  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  render() {
    const { width } = this.state;
    return (
      <div className="movies">
        <div className="first-icons">
          <span className="theatre-title">فیلم های بر روی تئاتر</span>
          <span className="see-all">دیدن همه</span>
        </div>

        <Slider1 width={width} />

        <div className="first-icons movie-headers">
          <span className="theatre-title">به زودی</span>
          <span className="see-all">دیدن همه</span>
        </div>
        <span className="see-all">فردا</span>

        <Slider2 width={width} />

        <div className="first-icons movie-headers"></div>

        <span className="see-all">خرداد 16</span>
        <Slider3 width={width} />

        <div className="more">دیدن بیشتر</div>
        <div className="end"></div>
        <div className="margin"></div>

      </div>
    );
  }
}

export default OnTheatre;
