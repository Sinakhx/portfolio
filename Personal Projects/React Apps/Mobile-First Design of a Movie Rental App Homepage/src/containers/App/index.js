import React, { Component } from 'react'
import Header from '../components/Header'
import '../assets/css/styles.css'
import OnTheatre from '../components/OnTheatre'

class index extends Component {
    render(){
        return (
            <div className="main-container">
                <Header />
                <OnTheatre />
            </div>
        )
    }
}

export default index