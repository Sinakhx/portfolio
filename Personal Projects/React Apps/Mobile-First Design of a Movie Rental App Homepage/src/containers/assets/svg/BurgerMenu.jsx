import React from 'react';

export const BurgerMenu = () => {
  return (
    <svg
      version="1.1"
      id="Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      x="0px"
      y="0px"
      viewBox="0 0 50 50"
      style={{enableBackground: 'new 0 0 50 50'}}
      xmlSpace="preserve"
      width="20px"
      className="BurgerMenu"
    >
      <g>
        <path className="st0" d="M0,0h50v50H0V0z" />
        <path
          className="st1"
          d="M50,43.5c0,1.7-1.3,3-3,3H3c-1.7,0-3-1.3-3-3s1.3-3,3-3h44C48.7,40.5,50,41.8,50,43.5z M47.5,3.5h-45
		C1.1,3.5,0,4.6,0,6v1c0,1.4,1.1,2.5,2.5,2.5h45C48.9,9.5,50,8.4,50,7V6C50,4.6,48.9,3.5,47.5,3.5z M47,22.5H18c-1.7,0-3,1.3-3,3
		s1.3,3,3,3h29c1.7,0,3-1.3,3-3S48.7,22.5,47,22.5z"
        />
      </g>
    </svg>
  );
};
