import React from "react";

export const VideoPlayerIcon = () => {
  return (
    <svg
      version="1.1"
      id="Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      x="0px"
      y="0px"
      viewBox="0 0 188 188"
      style={{ enableBackground: "new 0 0 188 188" }}
      xmlSpace="preserve"
      className="VideoPlayerIcon"
    >
      <g>
        <path
          className="vid-center"
          d="M127.7,87.4L76.8,57.9c-1.4-0.8-2.9-1.3-4.5-1.3c-4.1,0-7,3.3-7,8v58.8c0,4.7,2.9,8,7,8c1.6,0,3.2-0.5,4.5-1.3
		l50.9-29.4c2.8-1.6,4.3-4,4.3-6.6S130.5,88.9,127.7,87.4"
        />
        <path
          className="vid-round"
          d="M94,0C42.1,0,0,42.1,0,94c0,51.9,42.1,94,94,94c51.9-0.1,93.9-42.1,94-94C188,42.1,145.9,0,94,0z M94,186.4
		c-51.1,0-92.4-41.4-92.4-92.4S42.9,1.6,94,1.6C145,1.6,186.4,43,186.4,94C186.4,145.1,145.1,186.4,94,186.4z"
        />
      </g>
    </svg>
  );
};
