import React, { Component } from "react";
import DatePicker from "react-datepicker2";
import "antd/dist/antd.css";
import "../assets/css/styles.css";
import { Form, Radio, Input, Button } from "antd";

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 5 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 }
  }
};

class Register extends Component {
  state = {
    nameInput: { value: "", validateStatus: "", help: "" },
    lastNameInput: { value: "", validateStatus: "", help: "" },
    mobile: { value: "", validateStatus: "", help: "" },
    email: { value: "", validateStatus: "", help: "" },
    password: { value: "", validateStatus: "", help: "" },
    passwordRepeat: { value: "", validateStatus: "", help: "" },
    birthdate: { value: "", validateStatus: "error", help: "" },
    gender: { value: "", validateStatus: "error", help: "" },
    validation: false
  };

  fItem = (flabel, fname) => {
    return (
      <Form.Item
        label={flabel}
        hasFeedback
        validateStatus={this.state[fname].validateStatus}
        help={this.state[fname].help}
      >
        <Input
          placeholder={flabel}
          name={fname}
          value={this.state[fname].value}
          onChange={this.handleChange}
        />
      </Form.Item>
    );
  };

  handleChange = e => {
    const check = this.validateField(e.target.name, e.target.value);
    this.setState({
      [e.target.name]: {
        value: check[0],
        validateStatus: check[1],
        help: check[2]
      }
    });
  };

  validateField = (name, value) => {
    //check for name
    if (name === "nameInput") {
      const status = value.length <= 2 ? "error" : "success";
      const help =
        value.length <= 2 ? "نام باید بیشتر از دو حرف داشته باشد" : "";
      return [value, status, help];
    }
    //check for last name
    if (name === "lastNameInput") {
      const status = value.length <= 2 ? "error" : "success";
      const help =
        value.length <= 2 ? "نام خانوادگی باید بیشتر از دو حرف داشته باشد" : "";
      return [value, status, help];
    }
    //check for mobile
    if (name === "mobile") {
      const pass = /^(\+989|989|9|09)\d{9}$/.test(value);
      const status = pass ? "success" : "error";
      const help = pass
        ? ""
        : "لطفاً شماره موبایل خود را به طور صحیح وارد نمایید";
      return [value, status, help];
    }
    //check for email
    if (name === "email") {
      const pass = /\S+@\S+\.\S+/.test(value);
      const status = pass ? "success" : "error";
      const help = pass
        ? ""
        : "لطفاً آدرس ایمیل خود را به طور صحیح وارد نمایید";
      return [value, status, help];
    }
    //check for password
    if (name === "password") {
      const short = value.length < 6;
      const digits = /\d/.test(value);
      const lower = /[a-z]/.test(value);
      const upper = /[A-Z]/.test(value);
      const nonWords = /\W/.test(value);
      const sum = digits + lower + upper + nonWords;
      if (short)
        return [value, "error", "حداقل طول کلمه ی عبور باید ۶ کاراکتر باشد"];
      if (sum < 2) return [value, "warning", "خیلی ضعیف"];
      if (sum < 3) return [value, "warning", "ضعیف"];
      if (sum < 4) return [value, "success", "خوب"];
      if (sum < 5 && value.length > 7) return [value, "success", "خیلی قوی"];
      return [value, "success", "قوی"];
    }
    //check for password-repeat
    if (name === "passwordRepeat") {
      const check = value === this.state.password.value;
      console.log(this.state.password.value);
      const status = check ? "success" : "error";
      const help = check ? "" : "کلمه عبور فعلی با کلمه عبور قبلی مطابقت ندارد";
      return [value, status, help];
    }
    //check for birthdate goes to handleBirthday

    //check for gender goes to handleGender
  };

  handleBirthday = e => {
    const time = e._d;
    this.setState({
      birthdate: { value: time, validateStatus: "success", help: "" }
    });
  };

  onRadioChange = e => {
    this.setState({
      gender: { value: e.target.value, validateStatus: "success", help: "" }
    });
  };

 validateSubmit = () => {
     const output = {};
     const values = Object.keys(this.state).map(key => {
      output[key] = this.state[key].value;
      return this.state[key].validateStatus;
     })
     for (let i = 0; i < 7; i++){
       if (!values[i] || values[i] === 'error') return;
      }
       
    alert(JSON.stringify(output));
 }

  render() {
    const radioStyle = {
      display: 'block',
      height: '30px',
      lineHeight: '30px',
    };

    return (
      <div className="main">
        <Form {...formItemLayout}>
          <h1>فرم ثبت نام</h1>
          {this.fItem("نام", "nameInput")}
          {this.fItem("نام خانوادگی", "lastNameInput")}
          {this.fItem("موبایل", "mobile")}
          {this.fItem("ایمیل", "email")}
          {this.fItem("کلمه عبور", "password")}
          {this.fItem("تکرار کلمه عبور", "passwordRepeat")}
          <Form.Item
            label="تاریخ تولد"
            validateStatus={this.state.birthdate.validateStatus}
            help={this.state.birthdate.help}
          >
            <DatePicker
              placeholder="تاریخ تولد"
              name="birthdate"
              onChange={e => this.handleBirthday(e)}
              timePicker={false}
              isGregorian={false}
            />
          </Form.Item>

          <h4 className="gender-tag">جنسیت: </h4>
          <Radio.Group className="gender" onChange={this.onRadioChange} value={this.state.gender.value}>
            <Radio style={radioStyle} value={'مرد'}>
              مرد
            </Radio>
            <Radio style={radioStyle} value={'زن'}>
              زن
            </Radio>
          </Radio.Group>
          
          <Button type="primary" onClick={this.validateSubmit}>ثبت و ارسال</Button>

        </Form>
      </div>
    );
  }
}

export default Register;
