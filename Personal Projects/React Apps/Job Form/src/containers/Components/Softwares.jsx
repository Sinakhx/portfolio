import React, { Component } from "react";
import { Table, Input } from "antd";


const columns = [
    {
      title: "ردیف",
      dataIndex: "key"
    },
    {
      title: "نام زبان هایی که با آن ها کار می‌کنید",
      dataIndex: "langs"
    },
    {
      title: "سطح آشنایی",
      dataIndex: "proficiency"
    },
    {
      title: "ورژن",
      dataIndex: "version"
    },
    {
      title: "انواع فریم ورک ها و کتابخانه که می‌شناسید",
      dataIndex: "libs"
    },
    {
      title: "میزان آشنایی با آن ها",
      dataIndex: "skill"
    }
  ];
  const data = [
    {
      key: "1",
      langs: <Input placeholder="نام زبان ها" />,
      proficiency: <Input placeholder="سطح آشنایی" />,
      version: <Input placeholder="ورژن" />,
      libs:<Input placeholder="فریم ورک ها و کتابخانه ها" />,
      skill: <Input placeholder="میزان آشنایی" />
    },
    {
      key: "2",
      langs: <Input placeholder="نام زبان ها" />,
      proficiency: <Input placeholder="سطح آشنایی" />,
      version: <Input placeholder="ورژن" />,
      libs:<Input placeholder="فریم ورک ها و کتابخانه ها" />,
      skill: <Input placeholder="میزان آشنایی" />
    },
    {
      key: "3",
      langs: <Input placeholder="نام زبان ها" />,
      proficiency: <Input placeholder="سطح آشنایی" />,
      version: <Input placeholder="ورژن" />,
      libs:<Input placeholder="فریم ورک ها و کتابخانه ها" />,
      skill: <Input placeholder="میزان آشنایی" />
    },
    {
      key: "4",
      langs: <Input placeholder="نام زبان ها" />,
      proficiency: <Input placeholder="سطح آشنایی" />,
      version: <Input placeholder="ورژن" />,
      libs:<Input placeholder="فریم ورک ها و کتابخانه ها" />,
      skill: <Input placeholder="میزان آشنایی" />
    }
  ];
  

class Softwares extends Component {
  render() {
    return (
      <div>
        <Table columns={columns} dataSource={data} size="middle" />
      </div>
    );
  }
}

export default Softwares;