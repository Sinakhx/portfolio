import React, { Component } from "react";
import ImageUpload from "./helpers/ImageUpload";
import { Row, Col } from "antd";
import logo from "../assets/img/logo.png";
import { gDate } from "./helpers/Date";
import { toPersianDigit } from "./helpers/En_to_fa_Digits";
import '../assets/css/radioButtons.css';

class TopRow extends Component {
  render() {
    return (
      <Row>
        <Col className="date" span={7}>
          <Row>
            <Col span={12}>{toPersianDigit(gDate())}</Col>
            <Col className="h-info" span={12}>:تاریخ </Col>
          </Row>
          <Row>
            <Col span={12}>{toPersianDigit("1")}</Col>
            <Col className="h-info" span={12}>:شماره </Col>
          </Row>
          <Row>
            <Col className="control" span={12}>
              <form>
                <label className="radio">
                  ندارد &nbsp;
                </label>
                  <input className="form-radio" type="radio" name="addon" />
                <label className="radio">
                  دارد
                </label>
                  <input className="form-radio" type="radio" name="addon" defaultChecked />
              </form>
            </Col>
            <Col className="h-info" span={12}>:پیوست </Col>
          </Row>
        </Col>
        <Col span={11} className="logo">
          {<img className="image" alt="logo" src={logo} />}
          <h1>فرم استخدام</h1>
        </Col>
        <Col span={6} className="uploadimg">
          <ImageUpload />
        </Col>
      </Row>
    );
  }
}

export default TopRow;
