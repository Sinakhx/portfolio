function getDateNow() {
  const d = new Date();
  const month = "" + (d.getMonth() + 1);
  const day = "" + d.getDate();
  const year = d.getFullYear();

  return [year, month, day];
}

//convert to jalali
function dNow(year, month, day, f) {
  const g_days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  const j_days_in_month = [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29];

  const gy = year - 1600;
  const gm = month - 1;
  const gd = day - 1;

  let g_day_no =
    365 * gy + div(gy + 3, 4) - div(gy + 99, 100) + div(gy + 399, 400);

  for (let i = 0; i < gm; ++i) g_day_no += g_days_in_month[i];
  if (gm > 1 && ((gy % 4 === 0 && gy % 100 !== 0) || gy % 400 === 0))
    g_day_no++;
  /* leap and after Feb */
  g_day_no += gd;

  let j_day_no = g_day_no - 79;

  const j_np = div(j_day_no, 12053); /* 12053 = 365*33 + 32/4 */
  j_day_no = j_day_no % 12053;

  let jy = 979 + 33 * j_np + 4 * div(j_day_no, 1461); /* 1461 = 365*4 + 4/4 */

  j_day_no %= 1461;

  if (j_day_no >= 366) {
    jy += div(j_day_no - 1, 365);
    j_day_no = (j_day_no - 1) % 365;
  }

  for (var i = 0; i < 11 && j_day_no >= j_days_in_month[i]; ++i)
    j_day_no -= j_days_in_month[i];
  let jm = i + 1;
  let jd = j_day_no + 1;

  function div(x, y) {
    return Math.floor(x / y);
  }

  if (jm < 10) jm = "0" + jm;
  if (jd < 10) jd = "0" + jd;

  if (!f || f === undefined) return { y: jy, m: jm, d: jd };
  else return jy + "/" + jm + "/" + jd;
}

const now = getDateNow();

export const gDate = () => {
  return dNow(...now, 1);
};