import React, { Component } from "react";
import { Table, Input } from "antd";


const columns = [
    {
      title: "ردیف",
      dataIndex: "key"
    },
    {
      title: "نام محل کار قبلی",
      dataIndex: "lastJobLocation"
    },
    {
      title: "تاریخ شروع",
      dataIndex: "startDate"
    },
    {
      title: "تاریخ پایان",
      dataIndex: "endDate"
    },
    {
      title: "نام سمت",
      dataIndex: "position"
    },
    {
      title: "میزان حقوق دریافتی",
      dataIndex: "income"
    },
    {
      title: "علت ترک شغل",
      dataIndex: "quitCause"
    }
  ];
  const data = [
    {
      key: "1",
      lastJobLocation: <Input placeholder="نام محل کار قبلی" />,
      startDate: <Input placeholder="تاریخ شروع" />,
      endDate: <Input placeholder="تاریخ پایان" />,
      position:<Input placeholder="نام سمت" />,
      income: <Input placeholder="میزان حقوق دریافتی" />,
      quitCause: <Input placeholder="علت ترک شغل" />,
    },
    {
      key: "2",
      lastJobLocation: <Input placeholder="نام محل کار قبلی" />,
      startDate: <Input placeholder="تاریخ شروع" />,
      endDate: <Input placeholder="تاریخ پایان" />,
      position:<Input placeholder="نام سمت" />,
      income: <Input placeholder="میزان حقوق دریافتی" />,
      quitCause: <Input placeholder="علت ترک شغل" />,
    },
    {
      key: "3",
      lastJobLocation: <Input placeholder="نام محل کار قبلی" />,
      startDate: <Input placeholder="تاریخ شروع" />,
      endDate: <Input placeholder="تاریخ پایان" />,
      position:<Input placeholder="نام سمت" />,
      income: <Input placeholder="میزان حقوق دریافتی" />,
      quitCause: <Input placeholder="علت ترک شغل" />,
    }
  ];
  

class LastJobs extends Component {
  render() {
    return (
      <div>
        <Table columns={columns} dataSource={data} size="middle" />
      </div>
    );
  }
}

export default LastJobs;