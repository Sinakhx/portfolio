import React from 'react';
import { Radio } from 'antd';

class LinearRadioBtns extends React.Component {
  state = {
    value: 1,
  };

  onChange = e => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,
    });
  };

  render() {
    return (
      <Radio.Group onChange={this.onChange} value={this.state.value}>
        <Radio value={1}>بلد نیستم</Radio>
        <Radio value={2}>ضعیف</Radio>
        <Radio value={3}>متوسط</Radio>
        <Radio value={4}>خوب</Radio>
        <Radio value={5}>عالی</Radio>
      </Radio.Group>
    );
  }
}

export default LinearRadioBtns;