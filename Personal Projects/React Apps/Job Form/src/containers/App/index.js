import React, { Component } from "react";
import "../assets/css/styles.css";
import TopRow from "../Components/TopRow";
import PersonalInfo from "../Components/PersonalInfo";


class index extends Component {
  render() {
    return (
      <div className="main">
        <TopRow />
        <PersonalInfo />
      </div>
    );
  }
}

export default index;
